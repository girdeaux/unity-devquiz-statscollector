// 20k per second, sooooo..
// Quite slow on my macbook at all, and there's probably a lot wiser and faster way to go about this...
// but alas, specs are specs.

class StatsCollector {
  constructor(batchLimit = 20000) {
    this.batchLimit = batchLimit < 3 ? 3 : batchLimit; // Yeah...
    this.batch = []; // Batch for which the values are calculated from
    this.counter = 0; // Batch counter
    this.median = null; // Current median
    this.mean = null; // Current average
    this.totalRequestsCounter = 0; // A spooky total
  }

  pushValue(responseTimeMs) {
    if (!responseTimeMs) {
      console.log("Tried to push null/undefined :<");
    }
    this.batch.push(responseTimeMs);

    this.counter++;
    this.totalRequestsCounter++;

    // Yeah, it's not really counting if actually 20k come in through one second,
    // but it's a good batch size for calculations still
    if (this.counter == this.batchLimit) {
      this.processBatch();
    }
  }

  processBatch() {
    console.log("Total requests:", this.totalRequestsCounter);
    const sorted = this.batch.sort((a, b) => a - b);

    let sum = 0;
    for (let i = 0; i < sorted.length; i++) {
      sum = sum + sorted[i]; // Fastest?
    }

    // Even though we have an even 20k dataset expected,
    // check just in case what we need for median calculation
    let middle = sorted.length / 2;
    const isInt = middle % 1 === 0;

    let mean = sum / sorted.length;
    let median = null;

    if (isInt) {
      const lowIndex = ~~middle;
      const highIndex = lowIndex + 1;
      median = (sorted[lowIndex] + sorted[highIndex]) / 2;
    } else {
      median = sorted[~~middle];
    }

    if (mean === null || median === null) {
      throw new Error("mean/median is null: should never happen");
    }

    this.mean = this.mean ? ((this.mean + mean) / 2) : mean;
    this.median = this.median ? ((this.median + median) / 2) : median;
    this.counter = 0; // Reset the batch counter
  }

  getMedian() {
    return this.median;
  }

  getAverage() {
    return this.mean;
  }
}

module.exports = {
  StatsCollector: StatsCollector
};
