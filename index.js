const StatsCollector = require('./stats_collector').StatsCollector;

// Some nice helpers

// Run a function n times
const times = (fn, multiplier) => {
  for (let i = 0; i < multiplier; i++) {
    fn();
  }
};

// Get a random number between two numbers
const randomBetween = (min, max) => Math.floor(Math.random() * max) + min;

// Uncomment the blocks to see stuff happening on the screen

// Simple test
const sc = new StatsCollector(5);
sc.pushValue(9);
sc.pushValue(10);
sc.pushValue(5);
sc.pushValue(6);
sc.pushValue(11);

// Something a lot worse. Should run in seconds.
// const sc = new StatsCollector();
// times(() => sc.pushValue(randomBetween(1, 19000)), 1000000);

// For a week! Will take forever...
// const sc = new StatsCollector();
// const sevenDaysInSeconds = 60 * 60 * 24 * 7;
// const totalRequestsInSevenDays = sevenDaysInSeconds * 20000;
// times(() => sc.pushValue(randomBetween(1, 19000)), totalRequestsInSevenDays);

console.log("Current median", sc.getMedian());
console.log("Current average (mean)", sc.getAverage());
