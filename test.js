const StatsCollector = require('./stats_collector').StatsCollector;

// Chai stuff
const chai = require('chai');
chai.should();
const expect = chai.expect;
const assert = chai.assert;

describe('StatsCollector()', () => {
  it('pushValue() pushes responseTime to collector', () => {
    const sc = new StatsCollector();
    sc.counter.should.equal(0);
    sc.pushValue(666);
    sc.counter.should.equal(1);
    sc.batch.should.deep.equal([666]);
    sc.pushValue(777);
    sc.counter.should.equal(2);
    sc.batch.should.deep.equal([666, 777]);
  });

  it('getMedian() returns median when batch is full, null when not', () => {
    const sc = new StatsCollector(3);
    expect(sc.getMedian()).to.be.null;
    sc.pushValue(5);
    sc.pushValue(10);
    sc.pushValue(12);
    sc.getMedian().should.equal(10);
  });

  it('getAverage() returns mean when batch is full, null when not', () => {
    const sc = new StatsCollector(3);
    expect(sc.getAverage()).to.be.null;
    sc.pushValue(5);
    sc.pushValue(10);
    sc.pushValue(12);
    sc.getAverage().should.equal(9);
  });

  it('getAverage() and getMedian() return last batch values if new batch is not full', () => {
    const sc = new StatsCollector(3);
    expect(sc.getAverage()).to.be.null;
    sc.pushValue(5);
    sc.pushValue(10);
    sc.pushValue(12);
    sc.getAverage().should.equal(9);
    sc.pushValue(100000);
    sc.getMedian().should.equal(10);
  });

  it('calculates mean and median correctly', () => {
    const sc = new StatsCollector(5);
    sc.pushValue(9);
    sc.pushValue(10);
    sc.pushValue(5);
    sc.pushValue(6);
    sc.pushValue(11);

    sc.getMedian().should.equal(9);
    sc.getAverage().should.equal(8.2);
  });

  it('calculates mean and median correctly with multiple batches', () => {
    const sc = new StatsCollector(5);
    sc.pushValue(9);
    sc.pushValue(10);
    sc.pushValue(5);
    sc.pushValue(6);
    sc.pushValue(11);

    sc.getMedian().should.equal(9);
    sc.getAverage().should.equal(8.2);

    sc.pushValue(10);
    sc.pushValue(20);
    sc.pushValue(70);
    sc.pushValue(80);
    sc.pushValue(120);
    // second batch mean/median = 60/70; sum 300

    sc.getMedian().should.equal(12.25);
    sc.getAverage().should.equal(21.15);
  });
});
